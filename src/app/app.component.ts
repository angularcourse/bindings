import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
  
  <h1>My First Angular App</h1>

  <img [src]="imageUrl" (click)='myMethod($event)'>
  <img [src]="imageUrl" on-mouseout='myMethod2()'>

  <input (keyup)='userInput($event)'>  

  <p>{{myType}}</p>

  `
})
export class AppComponent {
  myType ='';
  imageUrl = 'https://cdn-images-1.medium.com/max/1600/1*nbJ41jD1-r2Oe6FsLjKaOg.png';

  myMethod(event:any) {
    console.log('I was clicked')
  }

  myMethod2(event:any) {
    console.log('You hovered off!')
  }

  userInput(event:string){
    this.myType = event.target.value;
  }

}
